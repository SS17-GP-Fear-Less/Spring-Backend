FROM openjdk:8
ENV JAR_NAME gs-accessing-data-rest-0.1.0.jar
COPY ./build/libs/$JAR_NAME /usr/src/myapp/$JAR_NAME
WORKDIR /usr/src/myapp

EXPOSE 8080

CMD java -jar  ${JAR_NAME}
