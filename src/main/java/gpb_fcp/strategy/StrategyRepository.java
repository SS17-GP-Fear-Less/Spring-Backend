package gpb_fcp.strategy;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "strategies", path = "strategies")
public interface StrategyRepository extends PagingAndSortingRepository<Strategy, Long> {

}