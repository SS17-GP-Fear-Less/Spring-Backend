package gpb_fcp.comment;

import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PutMapping;

@BasePathAwareController
public class CommentController {
    private final CommentRepository commentRepository;

    public CommentController(CommentRepository commentRepository) {
        this.commentRepository = commentRepository;
    }

    @PutMapping(value = "/comments")
    public ResponseEntity<?> preventPut() {
        return new ResponseEntity<>(HttpStatus.METHOD_NOT_ALLOWED);
    }

    @DeleteMapping(value = "/comments")
    public ResponseEntity<?> preventDelete() {
        return new ResponseEntity<>(HttpStatus.METHOD_NOT_ALLOWED);
    }
}