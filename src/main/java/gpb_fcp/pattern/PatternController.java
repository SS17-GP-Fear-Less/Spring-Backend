package gpb_fcp.pattern;

import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;

@BasePathAwareController
public class PatternController {

  private final PatternRepository patternRepository;

  PatternController(PatternRepository patternRepository) {
    this.patternRepository = patternRepository;
  }

  @PutMapping(value = "/patterns")
  public ResponseEntity<?> preventsPut() {
    return new ResponseEntity<>(HttpStatus.METHOD_NOT_ALLOWED);
  }

  @PostMapping(value = "/patterns/{id}")
  public ResponseEntity<?> preventsPost() {
    return new ResponseEntity<>(HttpStatus.METHOD_NOT_ALLOWED);
  }

  @DeleteMapping(value = "/patterns")
  public ResponseEntity<?> deleteAllPatterns() {
    this.patternRepository.deleteAll();
    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
  }

}
