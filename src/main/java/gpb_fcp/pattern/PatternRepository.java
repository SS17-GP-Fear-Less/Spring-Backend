package gpb_fcp.pattern;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "patterns", path = "patterns")
public interface PatternRepository extends PagingAndSortingRepository<Pattern, Long> {

}