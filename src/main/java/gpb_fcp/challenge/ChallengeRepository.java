package gpb_fcp.challenge;


import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "challenges", path = "challenges")
public interface ChallengeRepository extends PagingAndSortingRepository<Challenge, Long> {

}