package gpb_fcp.challenge;

import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;

@BasePathAwareController
public class ChallengeController {

  private final ChallengeRepository challengeRepository;

  ChallengeController(ChallengeRepository challengeRepository) {
    this.challengeRepository = challengeRepository;
  }

  @PostMapping(value = "/challenges/{id}")
  public ResponseEntity<?> preventsPost() {
    return new ResponseEntity<>(HttpStatus.METHOD_NOT_ALLOWED);
  }

  @DeleteMapping(value = "/challenges")
  public ResponseEntity<?> deleteAllPatterns() {
    this.challengeRepository.deleteAll();
    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
  }
}
