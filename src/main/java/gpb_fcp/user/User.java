package gpb_fcp.user;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import gpb_fcp.comment.Comment;
import gpb_fcp.challenge.Challenge;
import gpb_fcp.strategy.Strategy;

@Entity
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @OneToMany(mappedBy = "creator")
    private List<Strategy> createdStrategies;

    @OneToMany(mappedBy = "creator")
    private List<Challenge> createdChallenges;

    @OneToMany(mappedBy = "creator")
    private List<Comment> givenComments;

    private String username;
    private String password;

    public User() {}

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public List<Comment> getGivenComments() {
        return givenComments;
    }

    public void setGivenComments(List<Comment> givenComments) {
        this.givenComments = givenComments;
    }

    public List<Challenge> getCreatedChallenges() {
        return createdChallenges;
    }

    public void setCreatedChallenges(List<Challenge> createdChallenges) {
        this.createdChallenges = createdChallenges;
    }

    public List<Strategy> getCreatedStrategies() {
        return createdStrategies;
    }

    public void setCreatedStrategies(List<Strategy> createdStrategies) {
        this.createdStrategies = createdStrategies;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}